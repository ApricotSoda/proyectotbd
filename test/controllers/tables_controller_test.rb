require 'test_helper'

class TablesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get tables_index_url
    assert_response :success
  end

  test "should get new" do
    get tables_new_url
    assert_response :success
  end

  test "should get edit" do
    get tables_edit_url
    assert_response :success
  end

  test "should get show" do
    get tables_show_url
    assert_response :success
  end

  test "should get delete" do
    get tables_delete_url
    assert_response :success
  end

end
