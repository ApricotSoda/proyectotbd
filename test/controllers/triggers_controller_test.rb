require 'test_helper'

class TriggersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get triggers_index_url
    assert_response :success
  end

  test "should get new" do
    get triggers_new_url
    assert_response :success
  end

  test "should get edit" do
    get triggers_edit_url
    assert_response :success
  end

  test "should get show" do
    get triggers_show_url
    assert_response :success
  end

  test "should get delete" do
    get triggers_delete_url
    assert_response :success
  end

end
