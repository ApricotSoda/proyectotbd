require 'test_helper'

class IndexesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get indexes_index_url
    assert_response :success
  end

  test "should get new" do
    get indexes_new_url
    assert_response :success
  end

  test "should get edit" do
    get indexes_edit_url
    assert_response :success
  end

  test "should get show" do
    get indexes_show_url
    assert_response :success
  end

  test "should get delete" do
    get indexes_delete_url
    assert_response :success
  end

end
