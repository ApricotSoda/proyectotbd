class ViewsController < ApplicationController
  def index
  end

  def new
  end

  def edit
    @oscar = params[:id]
  end

  def show
    @oscar = params[:id]
  end

  def delete
    @oscar = params[:id]
  end

  def new2
    @sql= params[:sqls]
    ActiveRecord::Base.connection.exec_query(@sql)
  end

  def edit2
    @sql= params[:sqls]
    ActiveRecord::Base.connection.exec_query(@sql)
  end

  def delete2
    @oscar= params[:id]
    @sql = "DROP VIEW "+@oscar
    ActiveRecord::Base.connection.exec_query(@sql)
  end
end
