Rails.application.routes.draw do
  get 'menus/index'

  post 'menus/index', to: 'menus#index1'

  get 'menus/lobby'

  get 'procedures/index'

  get 'procedures/new'

  post 'procedures/new', to: 'procedures#new2'

  post 'procedures/edit/:id', to: 'procedures#edit2'

  post 'procedures/delete/:id', to: 'procedures#delete2'

  get 'procedures/edit/:id' => 'procedures#edit', as: 'procedures_edit'

  get 'procedures/show/:id' => 'procedures#show', as: 'procedures_show'

  get 'procedures/delete/:id' => 'procedures#delete', as: 'procedures_delete'

  get 'checks/index'

  get 'checks/new'

  post 'checks/new', to: 'checks#new2'

  post 'checks/edit/:id', to: 'checks#edit2'

  post 'checks/delete/:id', to: 'checks#delete2'

  get 'checks/edit/:id' => 'checks#edit', as: 'checks_edit'

  get 'checks/show/:id' => 'checks#show', as: 'checks_show'

  get 'checks/delete/:id' => 'checks#delete', as: 'checks_delete'

  get 'triggers/index'

  get 'triggers/new'

  post 'triggers/new', to: 'triggers#new2'

  post 'triggers/edit/:id', to: 'triggers#edit2'

  post 'triggers/delete/:id', to: 'triggers#delete2'

  get 'triggers/edit/:id' => 'triggers#edit', as: 'triggers_edit'

  get 'triggers/show/:id' => 'triggers#show', as: 'triggers_show'

  get 'triggers/delete/:id' => 'triggers#delete', as: 'triggers_delete'

  get 'indexes/index'

  get 'indexes/new'

  post 'indexes/new', to: 'indexes#new2'

  post 'indexes/edit/:id', to: 'indexes#edit2'

  post 'indexes/delete/:id', to: 'indexes#delete2'

  get 'indexes/edit/:id' => 'indexes#edit', as: 'indexes_edit'

  get 'indexes/show/:id' => 'indexes#show', as: 'indexes_show'

  get 'indexes/delete/:id' => 'indexess#delete', as: 'indexes_delete'

  get 'views/index'

  get 'views/new'

  post 'views/new', to: 'views#new2'

  post 'views/edit/:id', to: 'views#edit2'

  post 'views/delete/:id', to: 'views#delete2'

  get 'views/edit/:id' => 'views#edit', as: 'views_edit'

  get 'views/show/:id' => 'views#show', as: 'views_show'

  get 'views/delete/:id' => 'views#delete', as: 'views_delete'

  get 'tables/index'

  get 'tables/new'

  post 'tables/new', to: 'tables#new2'

  post 'tables/edit/:id', to: 'tables#edit2'

  post 'tables/delete/:id', to: 'tables#delete2'

  get 'tables/edit/:id' => 'tables#edit', as: 'tables_edit'

  get 'tables/show/:id' => 'tables#show', as: 'tables_show'

  get 'tables/delete/:id' => 'tables#delete', as: 'tables_delete'

  get 'users/index'

  get 'users/new'
  
  post 'users/new', to: 'users#new2'

  post 'users/edit/:id', to: 'users#edit2'

  post 'users/delete/:id', to: 'users#delete2'

  get 'users/edit/:id' => 'users#edit', as: 'users_edit'

  get 'users/show/:id' => 'users#show', as: 'users_show'

  get 'users/delete/:id' => 'users#delete', as: 'users_delete'

  get 'user/show'

  get 'user/new'

  get 'user/edit'

  get 'user/delete'

  get 'user/index'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
